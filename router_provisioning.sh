
# opkg remove wpad-mini
# opkg install wpad
# uci del dhcp.lan.ra_slaac
# uci set dhcp.lan.ra='hybrid'
# uci set dhcp.lan.dhcpv6='hybrid'
# uci set dhcp.lan.force='1'
# uci set network.lan.proto='static'
# uci set network.lan.ipaddr='192.168.57.1'
# uci set network.lan.netmask='255.255.255.0'
# uci set network.lan.gateway='192.168.1.1'
# uci set network.lan.broadcast='192.168.57.255'
sudo opkg update
sudo uci del dhcp.lan.ra_slaac
sudo uci set dhcp.lan.ra='hybrid'
sudo uci set dhcp.lan.dhcpv6='hybrid'
sudo uci set network.lan.proto='static'
sudo uci set network.lan.ipaddr='192.168.56.2'
sudo uci set network.lan.netmask='255.255.255.0'
sudo uci set network.lan.gateway='192.168.1.1'
sudo uci commit && service network restart